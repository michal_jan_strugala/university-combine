package solver;

import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Michal on 11.04.17.
 */
public class CnfBuilderTest {

    private CnfBuilder cnfBuilder;

    @Before
    public void initialize() {
        cnfBuilder = new CnfBuilder();
    }

    @Test
    public void testCreatingCnfText() {
        String expected = "p cnf 4 2\n2 -3 0\n-2 4 5 0";
        String actual = cnfBuilder
                .append("A")
                .append(Operator.OR)
                .append(Operator.NOT)
                .append("B")
                .terminate()
                .append(Operator.NOT)
                .append("A")
                .append(Operator.OR)
                .append("C")
                .append(Operator.OR)
                .append("D")
                .terminate()
                .toString();

        assertThat(actual).isEqualTo(expected);
    }
}