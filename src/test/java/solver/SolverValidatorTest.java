package solver;

import com.google.common.io.Resources;
import org.junit.Before;
import org.junit.Test;

import java.io.File;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Michal on 17.04.17.
 */
public class SolverValidatorTest {

    private final String filepath = "das1.xls";
    private File file;
    private SolverValidator solverValidator;

    @Before
    public void initialize() {
        solverValidator = new SolverValidator();
        file = new File(Resources.getResource(filepath).getFile());
    }

    @Test
    public void validateInputFileWithProperStates() {
        String result = solverValidator.validateInputFile(file, 2);
        assertThat(result).isEqualTo(String.valueOf(true));
    }

    @Test
    public void validateInputFileWithManyEntryStates() {
        String result = solverValidator.validateInputFile(file, 4);
        assertThat(result).contains("Wrong number of entry states");
    }

    @Test
    public void validateInputFileWithNoAcceptingStates() {
        String result = solverValidator.validateInputFile(file, 5);
        assertThat(result).contains("There are no accepting states");
    }

    @Test
    public void validateInputFileWithUnknownState() {
        String result = solverValidator.validateInputFile(file, 6);
        assertThat(result).isEqualTo(String.valueOf(false));
    }

    @Test
    public void validateInputFileWithEmptyTransitionFields() {
        String result = solverValidator.validateInputFile(file, 7);
        assertThat(result).isEqualTo(String.valueOf(false));
    }

    @Test
    public void validateInputFileWithEmptyEntryFields() {
        String result = solverValidator.validateInputFile(file, 8);
        assertThat(result).isEqualTo(String.valueOf(false));
    }

    @Test
    public void validateInputFileWithMalformedData() {
        String result = solverValidator.validateInputFile(file, 9);
        assertThat(result).contains("Wrong number of entry states");
    }

    @Test
    public void validateInputFileWithInputEmptyField() {
        String result = solverValidator.validateInputFile(file, 10);
        assertThat(result).contains("Empty input field");
    }

    @Test
    public void validateENasInput() {
        String result = solverValidator.validateInputFile(file, 11);
        assertThat(result).isEqualTo(String.valueOf(true));
    }

    //USER INPUT

    @Test
    public void validateWordInputWithProperData() {
        String result = solverValidator.validateWordInput("1010111001", file, 0);
        assertThat(result).isEqualTo(String.valueOf(true));
    }

    @Test
    public void validateWordInputWithWrongInput() {
        String result = solverValidator.validateWordInput("1010311002", file, 0);
        assertThat(result).isEqualTo(String.valueOf(false));
    }

    @Test
    public void validateWordInputWithInputEmptyField() {
        String result = solverValidator.validateWordInput("111111", file, 10);
        assertThat(result).contains("Empty input field");
    }

}