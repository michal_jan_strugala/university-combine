package automatas;

import com.google.common.io.Resources;
import dk.brics.automaton.Automaton;
import org.junit.Before;
import org.junit.Test;
import parser.ExcelParser;
import parser.MappedStates;

import java.io.File;
import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;
/**
 * Created by Michal on 06.05.17.
 */
public class AutomataConverterTest {

    private MappedStates mappedStates;
    private ExcelParser excelParser;

    @Before
    public void setUp() throws IOException {
        File excelFile = new File(Resources.getResource("das1.xls").getFile());
        excelParser = new ExcelParser();
        excelParser.setExcelConnection(excelFile);
    }

    @Test
    public void testConversionWithNoDeadAndEpsilon() {
        mappedStates = excelParser.importFromXls(0);
        Automaton result = AutomataConverter.convert(mappedStates);
        assertThat(result.getAcceptStates().size()).isEqualTo(1);
        assertThat(result.getStates().size()).isEqualTo(3);
        assertThat(result.isDeterministic()).isTrue();
    }

    @Test
    public void testConversionWithDeadState() {
        mappedStates = excelParser.importFromXls(2);
        Automaton result = AutomataConverter.convert(mappedStates);
        assertThat(result.getAcceptStates().size()).isEqualTo(2);
        assertThat(result.getStates().size()).isEqualTo(4);
        assertThat(result.isDeterministic()).isTrue();
    }

    @Test
    public void testConversionWithEpsilonsAndDead() {
        mappedStates = excelParser.importFromXls(11);
        Automaton result = AutomataConverter.convert(mappedStates);
        assertThat(result.getStates().size()).isEqualTo(4);
        assertThat(result.getAcceptStates().size()).isEqualTo(3);
        assertThat(result.isDeterministic()).isFalse();
    }

    @Test
    public void testConversionWithEpsilonTransition() {
        mappedStates = excelParser.importFromXls(12);
        Automaton result = AutomataConverter.convert(mappedStates);
        assertThat(result.getAcceptStates().size()).isEqualTo(1);
        assertThat(result.getStates().size()).isEqualTo(3);
        assertThat(result.isDeterministic()).isFalse();
    }
}