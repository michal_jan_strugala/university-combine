package parser;

import com.google.common.io.Resources;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.junit.Before;
import org.junit.Test;
import org.sat4j.reader.ParseFormatException;
import solver.SolverStateType;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Michal on 09.04.17.
 */
public class ExcelParserTest {

    private ExcelParser excelParser;
    private File excelFile;

    @Before
    public void initialize() {
        excelParser = new ExcelParser();
        excelFile = new File(Resources.getResource("das1.xls").getFile());
    }

    @Test
    public void testSettingUpExcelConnection() throws IOException {
        excelParser.setExcelConnection(excelFile);
    }

    @Test
    public void testParsingFile() throws IOException {
        excelParser.setExcelConnection(excelFile);
        MappedStates parsedMappedStates = excelParser.importFromXls(0);

        assertThat(parsedMappedStates.getStates().size()).isEqualTo(3);
    }

    @Test
    public void testDualStateAutomatas() throws IOException {
        excelParser.setExcelConnection(excelFile);
        MappedStates parsedMappedStates = excelParser.importFromXls(1);
        NamedState stateA = parsedMappedStates.getState("A");
        NamedState stateC = parsedMappedStates.getState("C");

        assertThat(stateA.getType()).isEqualTo((StateType.DUAL));
        assertThat(stateC.getType()).isEqualTo(StateType.ACCEPTING);
    }

    @Test
    public void testEmptyStateAutomatas() throws IOException {
        excelParser.setExcelConnection(excelFile);
        MappedStates parsedMappedStates = excelParser.importFromXls(2);
        NamedState empty = parsedMappedStates.getState("@");

        assertThat(empty.getType()).isEqualTo((StateType.EMPTY));
    }

    @Test
    public void testMultiStateAutomata() throws IOException {
        excelParser.setExcelConnection(excelFile);
        MappedStates parsedMappedStates = excelParser.importFromXls(3);
        NamedState stateA = parsedMappedStates.getState("A");
        Set<NamedState> states = parsedMappedStates.getStates();

        assertThat(stateA.getType()).isEqualTo((StateType.DUAL));
        assertThat(states.size()).isEqualTo(4);
    }

    @Test
    public void testMultiStateTransition() throws IOException {
        excelParser.setExcelConnection(excelFile);
        MappedStates parsedMappedStates = excelParser.importFromXls(11);
        NamedState stateA = parsedMappedStates.getState("A");
        assertThat(parsedMappedStates.getReactions(stateA).getReactions().size()).isEqualTo(3);
    }

    @Test
    public void testGettingStatesForValidation()
            throws IOException, InvalidFormatException, ParseFormatException {

        excelParser.setExcelConnection(excelFile);
        Map<SolverStateType, List<String>> states = excelParser.getStatesMap (0, false);

        assertThat(states.get(SolverStateType.ENTRY).size()).isEqualTo(3);
        assertThat(states.get(SolverStateType.FINISH).size()).isEqualTo(6);
    }
}