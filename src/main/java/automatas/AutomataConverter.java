package automatas;

import dk.brics.automaton.Automaton;
import dk.brics.automaton.State;
import dk.brics.automaton.StatePair;
import dk.brics.automaton.Transition;
import parser.MappedStates;
import parser.NamedState;

import java.util.*;

/**
 * Created by Michal on 06.05.17.
 */
public class AutomataConverter {

    public static Automaton convert(MappedStates parsed) {

        Map<String, State> finalStates = new LinkedHashMap<>();
        Automaton automaton = new Automaton();

        for(NamedState state : parsed.getStates()) {
            String name = state.getName();
            State current = getCurrentNode(finalStates, name);
            current.setAccept(state.isAccepting());

            putToFinalIfNotExistent(finalStates, name, current);
            setAsRootIfStarting(automaton, state, current);

            Map<String, Set<NamedState>> reactions = parsed.getReactions(state).getReactions();
            for (Map.Entry<String, Set<NamedState>> reaction : reactions.entrySet()) {
                char input = reaction.getKey().charAt(0);
                for (NamedState node : reaction.getValue()) {
                    State transitionState = getTransitionState(finalStates, node);
                    addAdjacentNode(automaton, current, input, transitionState);
                }
            }
        }
        return automaton;
    }

    private static void addAdjacentNode(Automaton automaton, State current, char input, State next) {
        if (isEpsilonTransition(input))
            addEpsilonTransition(automaton, current, next);
        else
            addNormalTransition(current, input, next);
    }

    private static boolean isEpsilonTransition(char input) {
        return input == 'e';
    }

    private static State getCurrentNode(Map<String, State> finalStates, String name) {
        return finalStates.containsKey(name)
                ? finalStates.get(name)
                : new State();
    }

    private static void setAsRootIfStarting(Automaton automaton, NamedState state, State current) {
        if (state.isStarting()) automaton.setInitialState(current);
    }

    private static void putToFinalIfNotExistent(Map<String, State> states, String name, State current) {
        if (!states.containsKey(name)) states.put(name, current);
    }

    private static void addNormalTransition(State current, char input, State transitionState) {
        Transition transition = new Transition(input, transitionState);
        current.addTransition(transition);
    }

    private static void addEpsilonTransition(Automaton automaton, State current, State next) {
        Collection<StatePair> pair = Collections.singletonList(new StatePair(current, next));
        automaton.addEpsilons(pair);
    }

    private static State getTransitionState(Map<String, State> finalStates, NamedState node) {
        State transitionState = finalStates.get(node.getName());
        if (transitionState == null) {
            transitionState = new State();
            transitionState.setAccept(node.isAccepting());
            finalStates.put(node.getName(), transitionState);
        }
        return transitionState;
    }
}
