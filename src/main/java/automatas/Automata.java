package automatas;


import dk.brics.automaton.Automaton;
import dk.brics.automaton.RegExp;
import org.omg.CORBA.Environment;
import parser.MappedStates;

/**
 * Created by Michal on 03.05.17.
 */
public class Automata {

    private Automaton automaton;
    private boolean isMinimal;

    public Automata(Automaton automaton) {
        this.automaton = automaton;
    }
    public Automata(MappedStates mappedStates) {
        automaton = AutomataConverter.convert(mappedStates);
    }

    public Automata toDFA() {
        automaton.determinize();
        assert automaton.isDeterministic();
        return this;
    }

    public Automata minimize() {
        automaton.minimize();
        isMinimal = true;
        return this;
    }

    public boolean run(String input) {
        return automaton.run(input);
    }
    public boolean isDeterministic() {
        return automaton.isDeterministic();
    }
    public boolean isMinimal() { return isMinimal; }
    public String getTransitionDescription() { return automaton.toString(); }

    @Override
    public String toString() {
        String type = automaton.isDeterministic() ? "DFA" : "NFA";
        type = String.format("%-20s %-10s", "Automata Type:", type);
        String states = String.format("%-20s %-10s", "States:", automaton.getStates().size());
        String accept = String.format("%-20s %-10s", "Accepting:", automaton.getAcceptStates().size());
        String minimisation = String.format("%-20s %-10s", "Is minimal:", String.valueOf(isMinimal));
        String example = String.format("%-20s %-10s","Shortest example:", automaton.getShortestExample(true));

        StringBuilder sb = new StringBuilder()
                .append(type)
                .append(System.lineSeparator())
                .append(states)
                .append(System.lineSeparator())
                .append(accept)
                .append(System.lineSeparator())
                .append(minimisation)
                .append(System.lineSeparator())
                .append(example);
        return sb.toString();
    }
}
