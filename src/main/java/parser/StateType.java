package parser;

/**
 * Created by doka on 2017-04-06.
 */
public enum StateType {
    STARTING, ACCEPTING, DUAL, NORMAL, EMPTY;

    public static StateType getType(String input){
        if(input.contains(">*") || input.contains("*>")) return DUAL;
        if(input.contains("@")) return EMPTY;
        if(input.contains(">")) return STARTING;
        if(input.contains("*")) return ACCEPTING;
        return NORMAL;
    }
}
