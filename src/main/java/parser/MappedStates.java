package parser;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

/**
 * Created by doka on 2017-04-06.
 */
public class MappedStates {
    private Map<NamedState, InputTransitions> representation = new HashMap<>();

    public InputTransitions getReactions(NamedState state){
        return representation.get(state);
    }

    public void addReactionOnInput(NamedState state, String input, NamedState reaction){
        if(representation.get(state)==null){
            throw new RuntimeException("You try to insert reaction into not existing state");
        }
        representation.get(state).addReactionToInput(input,reaction);
    }
    public void addState(NamedState state){
        if(!representation.keySet().contains(state)){
            representation.put(state,new InputTransitions());
        }else {
            throw new RuntimeException("State duplication occured during addState to automat");
        }
    }

    public NamedState getState(String name){
        Optional<NamedState> optional = representation.keySet().stream().filter(e->e.getName().equals(name)).findFirst();
        return optional.isPresent() ? optional.get() : null;
    }

    public Set<NamedState> getStates() {
        return representation.keySet();
    }
    public boolean containsStateByName(String name){
        Optional<NamedState> stateOpt = representation.keySet().stream().filter(e->e.getName().equals(name)).findFirst();
        return stateOpt.isPresent();
    }

    public boolean containsState(NamedState state){
        return representation.keySet().contains(state);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("parser.MappedStates : \n");

        for(Map.Entry<NamedState, InputTransitions> entry : representation.entrySet()){
                    sb
                    .append("(")
                    .append(entry.getKey().getType().toString().charAt(0))
                    .append(")")
                    .append(entry.getKey().getName())
                    .append(" | ");
            for(Map.Entry<String,Set<NamedState>> entry1 : entry.getValue().getReactions().entrySet()){
                sb.append("(" + entry1.getKey() + ")");
                for(NamedState react : entry1.getValue()){
                    sb.append(react.getName());
                }
                sb.append(" ");
            }
            sb.append("\n");
        }

        return sb.toString();
    }
}
