package parser;

/**
 * Created by doka on 2017-04-06.
 */
public class NamedState {
    private String name;
    private StateType type;

    public NamedState(String name) {
        this.name = name;
    }

    public NamedState(String name, StateType type) {
        this.name = name;
        this.type = type;
    }

    public void setType(StateType type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public StateType getType() {
        return type;
    }

    public boolean isAccepting() {
        return type == StateType.ACCEPTING || type == StateType.DUAL;
    }

    public boolean isStarting() {
        return type == StateType.STARTING || type == StateType.DUAL;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NamedState that = (NamedState) o;

        return name != null ? name.equals(that.name) : that.name == null;
    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "parser.NamedState{" +
                "name='" + name + '\'' +
                ", type=" + type +
                '}';
    }
}
