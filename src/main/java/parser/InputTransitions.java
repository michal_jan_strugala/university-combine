package parser;

import java.util.*;

/**
 * Created by doka on 2017-04-06.
 */
public class InputTransitions {
    private Map<String, Set<NamedState>> reactions = new HashMap<>();

    public void addReactionToInput(String input, NamedState reaction){
        Set<NamedState> reactSet = reactions.get(input);
        if (reactSet == null){
            Set<NamedState> newSet = new HashSet<>();
            newSet.add(reaction);
            reactions.put(input, newSet);
        } else {
            reactSet.add(reaction);
        }
    }
    public Map<String, Set<NamedState>> getReactions() {
        return reactions;
    }
}
