package parser;

import org.apache.commons.io.FilenameUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.sat4j.reader.ParseFormatException;
import solver.SolverStateType;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;

/**
 * Created by doka on 2017-04-06.
 */
public class ExcelParser {
    private Workbook workbook;

    public void setExcelConnection(File path) throws IOException {
        FileInputStream excelFile = new FileInputStream(path);
        String extension = FilenameUtils.getExtension(path.getName());
        workbook = extension.equals("xls")
                ? new HSSFWorkbook(excelFile)
                : new XSSFWorkbook(excelFile);
    }

    public int getSheetsCount() throws IOException {
        if(workbook == null) throw new IOException("Workbook not specified");
        return workbook.getNumberOfSheets();
    }

    public Map<SolverStateType, List<String>> getStatesMap(int sheetNumber, boolean isTransitionCheck)
            throws ParseFormatException {

        Map<SolverStateType, List<String>> result = new HashMap<>();
        Sheet dataSheet = workbook.getSheetAt(sheetNumber);
        Iterator<Row> rowIterator = dataSheet.rowIterator();

        if (isTransitionCheck) {
            List<String> finishList = getInputList(rowIterator);
            result.put(SolverStateType.FINISH, finishList);
        } else {
            rowIterator.next(); //skip input
            List<String> entries = new LinkedList<>();
            List<String> finishes = new LinkedList<>();
            fillStatesLists(rowIterator, entries, finishes);

            result.put(SolverStateType.ENTRY, entries);
            result.put(SolverStateType.FINISH, finishes);
        }
        return result;
    }

    private void fillStatesLists(Iterator<Row> rowIterator, List<String> entries, List<String> finishes)
            throws ParseFormatException {

        int entryCount = 0;
        int acceptingCount = 0;

        while(rowIterator.hasNext()) {
            int currentCol = 0;
            Row currentRow = rowIterator.next();

            for (Cell cell : currentRow) {
                List<String> cellValues = getCellValues(cell);
                if (cellValues == null) continue;
                for (String cellValue : cellValues) {
                    StateType type = StateType.getType(cellValue);
                    cellValue = adjustCellValueForState(cellValue, type);
                    if (cellValue.isEmpty()) continue;

                    if (currentCol == 0) {
                        if (type == StateType.ACCEPTING) acceptingCount++;
                        if (type == StateType.STARTING) entryCount++;
                        if (type == StateType.DUAL) { acceptingCount++; entryCount++; }
                        entries.add(cellValue);
                    } else {
                        finishes.add(cellValue);
                    }
                }
                currentCol++;
            }
        }

        if (entryCount != 1) throw new ParseFormatException("Wrong number of entry states");
        if (acceptingCount < 1) throw new ParseFormatException("There are no accepting states");
    }

    public MappedStates importFromXls(int sheetNumber) {
        Sheet datatypeSheet = workbook.getSheetAt(sheetNumber);
        Iterator<Row> rowIterator = datatypeSheet.iterator();
        MappedStates mappedStates = new MappedStates();

        List<String> inputList = null;
        try {
            inputList = getInputList(rowIterator);
        } catch (ParseFormatException e) {
            e.printStackTrace();
        }

        while (rowIterator.hasNext()) {
            Row currentRow = rowIterator.next();
            Iterator<Cell> cellIterator = currentRow.iterator();
            NamedState stateRow = null;
            int currentCol = 0;

            while (cellIterator.hasNext()) {
                Cell cell = cellIterator.next();
                List<String> cellValues = getCellValues(cell);
                if (cellValues == null) continue;
                for(String cellValue : cellValues) {
                    if (cellValue.isEmpty()) continue;
                    if (currentCol == 0) {
                        stateRow = parseEntryStates(mappedStates, cellValue);
                    } else {
                        NamedState react = getInsertedStateByName(mappedStates, cellValue, null);
                        mappedStates.addReactionOnInput(stateRow, inputList.get(currentCol - 1), react);
                    }
                }
                ++currentCol;
            }
        }
        addReactionsForEmptyState(mappedStates, inputList);
        return mappedStates;
    }

    private void addReactionsForEmptyState(MappedStates mappedStates, List<String> inputList) {
        NamedState state = mappedStates.getState("@");
        if(state != null) {
            for(String input : inputList) {
                mappedStates.addReactionOnInput(state, input, state);
            }
        }
    }

    public int getInputCount(int sheetNumber) throws ParseFormatException {
        Sheet datatypeSheet = workbook.getSheetAt(sheetNumber);
        Iterator<Row> rowIterator = datatypeSheet.iterator();
        return getInputList(rowIterator).size();
    }

    private NamedState parseEntryStates(MappedStates mappedStates, String cellValue) {
        StateType cellStateType = StateType.getType(cellValue);
        cellValue = adjustCellValueForState(cellValue, cellStateType);
        return getInsertedStateByName(mappedStates, cellValue, cellStateType);
    }

    private NamedState getInsertedStateByName(MappedStates mappedStates, String cellValue, StateType stateType) {
        NamedState result;
        if (!mappedStates.containsStateByName(cellValue)) {
            stateType = getStateByValue(cellValue, stateType);
            result = new NamedState(cellValue, stateType);
            mappedStates.addState(result);
        } else {
            result = mappedStates.getState(cellValue);
            if (stateType != null){
                result.setType(stateType);
            }
        }
        return result;
    }

    private StateType getStateByValue(String cellValue, StateType state) {
        if (state != null) return state;
        return cellValue.equals("@") ? StateType.EMPTY : StateType.NORMAL;
    }

    private String adjustCellValueForState(String cellValue, StateType cellState) {
        String resultValue = cellValue;
        switch (cellState) {
            case DUAL:
                resultValue = cellValue.substring(2);
                break;
            case ACCEPTING:
            case STARTING:
                resultValue = cellValue.substring(1);
                break;
            default:
                break;
        }
        return resultValue;
    }

    private List<String> getInputList(Iterator<Row> rowIterator) throws ParseFormatException {
        if(!rowIterator.hasNext()) return null;
        List<String> inputList = new ArrayList<>();
        Iterator<Cell> cellIterator = rowIterator.next().cellIterator();
        cellIterator.next(); //skip first cell
        while (cellIterator.hasNext()) {
            Cell currentCell = cellIterator.next();
            List<String> cellValues = getCellValues(currentCell);
            if(cellValues == null) throw new ParseFormatException("Empty input field");
            for(String cellValue : cellValues) {
                if(cellValue.isEmpty()) continue;
                inputList.add(cellValue);
            }
        }
        return inputList;
    }

    private List<String> getCellValues(Cell currentCell) {
        String value;
        switch (currentCell.getCellTypeEnum()) {
            case STRING:
                value = currentCell.getStringCellValue();
                break;
            case NUMERIC:
                value = String.valueOf((int) currentCell.getNumericCellValue());
                break;
            case BOOLEAN:
                value = String.valueOf(currentCell.getBooleanCellValue());
                break;
            case BLANK:
            case _NONE:
                value = null;
                break;
            default:
                throw new RuntimeException("Excel file has an inappropriate input format.");
        }
        return value == null
                ? null
                : Arrays.asList(value.split(","));
    }
}
