package controllers;

import javafx.scene.control.Alert;

/**
 * Created by Michal on 18.04.17.
 */
public class AlertFactory {

    public void create(Alert.AlertType type, String title, String header, String context) {
        Alert alert = createAlert(type, title, context);
        alert.setHeaderText(header);
        alert.showAndWait();
    }

    public void create(Alert.AlertType type, String title, String context) {
        Alert alert = createAlert(type, title, context);
        alert.setHeaderText(null);
        alert.showAndWait();
    }

    private Alert createAlert(Alert.AlertType type, String title, String context) {
        Alert alert = new Alert(type);
        alert.setTitle(title);
        alert.setContentText(context);

        return alert;
    }
}
