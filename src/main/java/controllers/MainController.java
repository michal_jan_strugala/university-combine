package controllers;

import automatas.Automata;
import com.google.common.io.Resources;
import com.google.common.primitives.Ints;
import dk.brics.automaton.RegExp;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.apache.commons.io.FilenameUtils;
import parser.ExcelParser;
import parser.MappedStates;
import solver.SolverValidator;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.IntStream;

/**
 * Created by Michal on 18.04.17.
 */
public class MainController implements Initializable {

    private Stage primaryStage;
    private String currentFilePath;
    private AlertFactory alertFactory;
    private ExcelParser excelParser;
    private Automata automata;
    private Application mainApp;
    private Operation currentOperation;
    private int sheetNumber;

    @FXML private MenuItem menuHelpDocumentation;
    @FXML private MenuItem menuFileClose;
    @FXML private Button fileChooseBtn;
    @FXML private Button parseFileBtn;
    @FXML private Button parseRegexBtn;
    @FXML private Button resetBtn;
    @FXML private Button runBtn;
    @FXML private Label inputWordLabel;
    @FXML private TextArea inputWordTxt;
    @FXML private TextArea automataDetailsArea;
    @FXML private TextArea automataTransitionArea;
    @FXML private TextField statusTxt;
    @FXML private TextField filenameTxt;
    @FXML private TextField regexTxt;
    @FXML private ComboBox<String> operationCombo;
    @FXML private ComboBox<Integer> sheetNumberCombo;

    public MainController() {
        alertFactory = new AlertFactory();
        excelParser = new ExcelParser();
    }

    public void setStage(Stage stage) {
        this.primaryStage = stage;
    }
    public void setMainApp(Application mainApp) {
        this.mainApp = mainApp;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        fileChooseBtn.setOnAction(event -> {
            FileChooser fileChooser = new FileChooser();
            File file = fileChooser.showOpenDialog(primaryStage);
            if (file != null) {
                try {
                    setSheetsCount(file);
                } catch (IOException e) {
                    e.printStackTrace();
                    alertFactory.create(Alert.AlertType.ERROR,
                            "File error",
                            "Could not access file",
                            e.getLocalizedMessage());
                }
            } else {
                parseFileBtn.setDisable(true);
            }
        });

        sheetNumberCombo.getSelectionModel().selectedItemProperty()
                .addListener((observable, oldValue, newValue) -> {
                    sheetNumber = newValue;
                 });

        resetBtn.setOnAction(event -> {
            currentFilePath = null;
            statusTxt.clear();
            filenameTxt.clear();
            regexTxt.clear();
            inputWordTxt.clear();
            operationCombo.getSelectionModel().selectFirst();
            inputWordLabel.setText("");
            inputWordTxt.clear();
            inputWordTxt.setDisable(true);
            parseFileBtn.setDisable(true);
            runBtn.setDisable(true);
            automataDetailsArea.clear();
        });

        parseFileBtn.setOnAction(event -> {
            File file = new File(currentFilePath);
            if (!FilenameUtils.getExtension(file.getName()).equals("xls")) {
                alertFactory.create(Alert.AlertType.ERROR,
                        "Wrong file extension",
                        "Use excel file(*.xls)");
            } else {
                validateAndParse(file);
            }
        });

        menuHelpDocumentation.setOnAction(e -> {
            if (Desktop.isDesktopSupported()) {
                String documentation = Resources.getResource("documentation/docs.pdf").getFile();
                mainApp.getHostServices().showDocument(documentation);
            }
        });

        parseRegexBtn.setOnAction(event -> {
            String regex = regexTxt.getText();
            parseRegex(regex);
        });

        operationCombo.getSelectionModel().selectedItemProperty()
                .addListener((observable, oldValue, newValue) -> {
                    currentOperation = Operation.getOperation(newValue);
                    if (currentOperation == Operation.TEST_WORD) {
                        inputWordLabel.setText("Input test word below");
                        inputWordTxt.setDisable(false);
                    } else {
                        inputWordLabel.setText("");
                        inputWordTxt.setDisable(true);
                    }
                });

        runBtn.setOnAction(event -> {
            switch (currentOperation) {
                case NFA_TO_DFA:
                    nfaToDfa();
                    break;
                case DFA_TO_MIN_DFA:
                    dfaToMinDfa();
                    break;
                case TEST_WORD:
                    runWord();
                    break;
            }
        });

        menuFileClose.setOnAction(e -> Platform.exit());

        operationCombo.getSelectionModel().selectFirst();
        inputWordLabel.setText("");
        runBtn.setDisable(true);
        parseFileBtn.setDisable(true);
        sheetNumberCombo.setDisable(true);
        automataTransitionArea.setDisable(false);
        automataTransitionArea.setEditable(false);
        automataTransitionArea.setStyle(""
                + "-fx-opacity: 1.0;"
                + "-fx-font-family: monospace;"
                + "-fx-text-fill: navy;");
        automataDetailsArea.setDisable(false);
        automataDetailsArea.setEditable(false);
        automataDetailsArea.setStyle(""
                + "-fx-font-size: 15px;"
                + "-fx-font-style: italic;"
                + "-fx-font-weight: bold;"
                + "-fx-font-family: monospace;"
                + "-fx-text-fill: maroon;"
                + "-fx-opacity: 1.0;");
    }

    private void parseRegex(String regex) {
        try{
            RegExp regExp = new RegExp(regex);
            automata = new Automata(regExp.toAutomaton());
            setAutomataStatus(true);
            alertFactory.create(Alert.AlertType.INFORMATION,
                    "Success",
                    "Regex was successfully parsed");
        }catch (IllegalArgumentException e) {
            setAutomataStatus(false);
            alertFactory.create(Alert.AlertType.ERROR,
                    "Regular expression error",
                    "Regexp is invalid. Please check syntax.",
                    e.getLocalizedMessage());
        }
    }

    private void runWord() {
        String word = inputWordTxt.getText();
        if(automata.run(word)) {
            alertFactory.create(Alert.AlertType.INFORMATION,
                    "Word accepted!",
                    "Specified word belongs to the language");
        } else {
            alertFactory.create(Alert.AlertType.INFORMATION,
                    "Word rejected!",
                    "Specified word is not part of the language");
        }
    }

    private void dfaToMinDfa() {
        if(automata.isMinimal()) {
            alertFactory.create(Alert.AlertType.ERROR,
                    "Conversion failed",
                    "DFA is already minimal");
        } else {
            automata.minimize();
            setAutomataStatus(true);
        }
    }

    private void nfaToDfa() {
        if(automata.isDeterministic()) {
            alertFactory.create(Alert.AlertType.ERROR,
                    "Conversion failed",
                    "Automata is already a DFA");
        } else {
            automata.toDFA();
            setAutomataStatus(true);
        }
    }

    private void setSheetsCount(File file) throws IOException {
        currentFilePath = file.getAbsolutePath();
        filenameTxt.setText(Paths.get(currentFilePath).getFileName().toString());
        excelParser.setExcelConnection(file);
        int availableSheets = excelParser.getSheetsCount();
        int[] range = IntStream.rangeClosed(0, availableSheets-1).toArray();
        List<Integer> list = Ints.asList(range);
        sheetNumberCombo.getItems().removeAll();
        sheetNumberCombo.getItems().addAll(list);
        sheetNumberCombo.setValue(0);
        parseFileBtn.setDisable(false);
        sheetNumberCombo.setDisable(false);
    }

    private void validateAndParse(File file) {
        SolverValidator sv = new SolverValidator();
        String result = sv.validateInputFile(file, sheetNumber);
        if (result != String.valueOf(true)) {
            alertFactory.create(Alert.AlertType.ERROR,
                    "File format error",
                    "Specified data did not pass validation tests. Result:",
                    result);
        } else {
            parseAutomata();
        }
    }

    private void parseAutomata() {
        try {
            MappedStates mappedStates = excelParser.importFromXls(sheetNumber);
            automata = new Automata(mappedStates);
            setAutomataStatus(true);
            alertFactory.create(Alert.AlertType.INFORMATION,
                    "Success",
                    "File was successfully parsed");
        } catch (Exception e) {
            setAutomataStatus(false);
            alertFactory.create(Alert.AlertType.ERROR,
                    "Parse error",
                    "File couldn't be parsed",
                    e.getLocalizedMessage());
        }
    }

    private void setAutomataStatus(boolean success) {
        if(success){
            String type = automata.isDeterministic() ? "DFA" : "NFA";
            runBtn.setDisable(false);
            statusTxt.setText("Created " + type);
            automataDetailsArea.setText(automata.toString());
            automataTransitionArea.setText(automata.getTransitionDescription());
        } else {
            runBtn.setDisable(true);
            automata = null;
            automataDetailsArea.clear();
            automataTransitionArea.clear();
        }
    }
}
