package controllers;

/**
 * Created by Michal on 06.05.17.
 */
public enum Operation {
    NFA_TO_DFA, DFA_TO_MIN_DFA, TEST_WORD;

    public static Operation getOperation(String operation) {
        if (operation.toLowerCase().contains("nfa")) return NFA_TO_DFA;
        if (operation.toLowerCase().contains("min")) return DFA_TO_MIN_DFA;
        return TEST_WORD;
    }
}
