package solver;

import org.sat4j.minisat.SolverFactory;
import org.sat4j.reader.DimacsReader;
import org.sat4j.reader.ParseFormatException;
import org.sat4j.reader.Reader;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IProblem;
import org.sat4j.specs.ISolver;
import org.sat4j.specs.TimeoutException;
import org.sat4j.tools.ModelIterator;
import parser.ExcelParser;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Michal on 09.04.17.
 */

public class SolverValidator {

    public String validateInputFile(File file, int sheetNumber) {
        int modelCount = 0;
        try {
            IProblem problem = createProblemInstance(file, sheetNumber);

            while(problem.isSatisfiable()) {
                problem.model();
                modelCount++;
            }

        } catch(ArrayIndexOutOfBoundsException e) {
            return String.valueOf(false);
        } catch (ContradictionException | IOException | TimeoutException e) {
            return String.valueOf(false);
        } catch (ParseFormatException e ) {
            return e.getMessage();
        }

        return String.valueOf(modelCount == 1);
    }

    private IProblem createProblemInstance(File file, int sheetNumber)
            throws ParseFormatException, IOException, ContradictionException {

        String cnfString = getCnfNotationString(file, sheetNumber);
        byte problemSpecification[] = cnfString.getBytes(StandardCharsets.UTF_8);
        InputStream stream = new ByteArrayInputStream(problemSpecification);
        ISolver solver = SolverFactory.newDefault();
        ModelIterator mi = new ModelIterator(solver);
        Reader reader = new DimacsReader(mi);

        return reader.parseInstance(stream);
    }

    public String validateWordInput(String input, File file, int sheetNumber) {
        ExcelParser parser = new ExcelParser();
        Set<String> userInputStates = uniqueUserInputStates(input);
        List<String> fileInputList;
        try {
            parser.setExcelConnection(file);
            fileInputList = parser.getStatesMap(sheetNumber, true).get(SolverStateType.FINISH);
        } catch (IOException | ParseFormatException e) {
            return e.getMessage();
        }
        Set<String> fileInputStates = new LinkedHashSet<>(fileInputList);

        return String.valueOf(userInputStates.equals(fileInputStates));
    }

    private String getCnfNotationString(File file, int sheetNumber)
            throws ParseFormatException, IOException {

        ExcelParser parser = new ExcelParser();
        parser.setExcelConnection(file);
        Map<SolverStateType, List<String>> states = parser.getStatesMap(sheetNumber, false);

        int inputCount = parser.getInputCount(sheetNumber);
        List<String> transitions = states.get(SolverStateType.FINISH);
        String transitionStates[] = transitions.toArray(new String [transitions.size()]);
        CnfBuilder cnfBuilder = new CnfBuilder().addIndependentSymbols();

        int rowCounter = 0;
        for(String primaryState : states.get(SolverStateType.ENTRY)) {
            cnfBuilder.append(primaryState).terminate();
            for(int i = 0; i < inputCount; i++) {
                cnfBuilder.append(Operator.NOT);
                cnfBuilder.append(transitionStates[rowCounter * inputCount + i]);
                cnfBuilder.append(Operator.OR).append(primaryState);
                cnfBuilder.terminate();
            }
            rowCounter++;
        }

        return cnfBuilder.toString();
    }

    private Set<String> uniqueUserInputStates(String input) {
        Set<String> uniqueCollection = new LinkedHashSet<>();
        for (int i = 0; i < input.length(); i++){
            uniqueCollection.add(input.substring(i, i+1));
        }
        return uniqueCollection;
    }
}
