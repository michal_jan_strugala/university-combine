package solver;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Michal on 10.04.17.
 */
public class CnfBuilder {

    private StringBuilder sb;
    private Map<String, Integer> variables;
    private int formulaRows;

    public CnfBuilder() {
        sb = new StringBuilder();
        variables = new HashMap<>();
        formulaRows = 0;
    }

    public CnfBuilder addIndependentSymbols() {
        this.append("@").terminate();
        this.append("e").terminate();

        return this;
    }

    public CnfBuilder append(Operator operator) {
        switch (operator) {
            case AND:
                sb.append("\n");
                break;
            case OR:
                sb.append(" ");
                break;
            case NOT:
                sb.append("-");
                break;
        }
        return this;
    }

    public CnfBuilder append(String variable) {
        addVarIfNotExisting(variable);
        int solverVar = variables.get(variable);
        sb.append(solverVar);
        return this;
    }

    public CnfBuilder terminate() {
        sb.append(" 0\n");
        formulaRows++;
        return this;
    }

    @Override
    public String toString() {
        return this.toCNF().sb.toString().trim();
    }

    private CnfBuilder toCNF() {
        String specification = String.format("p cnf %d %d\n", variables.keySet().size(), formulaRows);
        sb.insert(0, specification);
        return this;
    }

    private void addVarIfNotExisting(String variable) {
        if(!variables.containsKey(variable))
            variables.put(variable, (int) variable.charAt(0) - 63);
    }
}
