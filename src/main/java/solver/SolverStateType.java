package solver;

/**
 * Created by Michal on 09.04.17.
 */
public enum SolverStateType {
    ENTRY, FINISH
}
