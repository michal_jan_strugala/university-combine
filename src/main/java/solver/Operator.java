package solver;

/**
 * Created by Michal on 10.04.17.
 */
public enum Operator {
    AND, OR, NOT
}
